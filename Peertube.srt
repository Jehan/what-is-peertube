﻿1
00:00:00,526 --> 00:00:02,143
What is PeerTube?

2
00:00:01,912 --> 00:00:04,565
Peertube is an open-source video platform.

3
00:00:05,090 --> 00:00:06,805
Like YouTube or Vimeo

4
00:00:06,805 --> 00:00:10,347
you can watch, comment and upload videos online

5
00:00:10,803 --> 00:00:11,861
but there's more…

6
00:00:11,861 --> 00:00:14,267
it empowers you to be more than a user!

7
00:00:15,300 --> 00:00:19,081
You can make the web yours, by installing your own PeerTube!

8
00:00:21,521 --> 00:00:23,552
PeerTube instances can sync together.

9
00:00:24,336 --> 00:00:26,681
When Meow-tube chooses to sync with Dog-tube…

10
00:00:26,681 --> 00:00:30,214
all of Dog-tube videos can be watched on Meow-tube!

11
00:00:30,676 --> 00:00:35,613
The video files stay on Dog-tube server while being streamed seamlessly to Meow-tube.

12
00:00:37,252 --> 00:00:39,924
This federation approach creates a video network

13
00:00:39,924 --> 00:00:43,885
with shared video-catalog without paying for more disk space.

14
00:00:44,741 --> 00:00:46,084
This is the base of the web!

15
00:00:49,686 --> 00:00:51,893
You don't need a data-center to run PeerTube.

16
00:00:52,646 --> 00:00:56,665
When someone watches a video, it is streamed directly from the PeerTube instance.

17
00:00:58,155 --> 00:01:01,154
When several people watch simultaneously the same video…

18
00:01:01,154 --> 00:01:04,625
bits of the video are shared between them in the background.

19
00:01:05,323 --> 00:01:08,851
Peer-to-peer streaming makes video streaming resilient and efficient.

20
00:01:09,554 --> 00:01:12,261
A successfull video shouldn't bring an instance down.

21
00:01:15,488 --> 00:01:18,300
Peertube is an open-source software with community-driven development.

22
00:01:19,707 --> 00:01:21,557
Institutions, medias, communities…

23
00:01:21,557 --> 00:01:24,687
anyone can make their own videotube

24
00:01:24,687 --> 00:01:26,846
with their own rules, at a low cost.

25
00:01:27,416 --> 00:01:32,932
In the long run, you'll be able to find and join the Peertube instance that really suits you.

26
00:01:33,943 --> 00:01:36,783
Make the web yours again, get its control back, one video at a time…

27
00:01:37,875 --> 00:01:38,720
Just join PeerTube.

