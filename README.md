# "What is PeerTube?" - an animation by Aryeom Han

This short animation was produced by the non-profit
[LILA](https://libreart.info), directed by Aryeom, assisted by Jehan, of
[ZeMarmot](https://film.zemarmot.net) project.

![What is PeerTube](https://gitlab.gnome.org/Jehan/what-is-peertube/raw/master/S4%20User%20freedom%20png/synfigS4/s4.0270.png)

It is a video to explain the base concepts behing
[PeerTube](https://joinpeertube.org/), a web software to play videos on
a website, commissioned by [Framasoft](https://framasoft.org/)
non-profit.
It can be watched:

* [In English](https://framatube.org/videos/watch/217eefeb-883d-45be-b7fc-a788ad8507d3)
* [With French subtitles](https://framatube.org/videos/watch/9db9f3f1-9b54-44ed-9e91-461d262d2205)

And downloaded from [TuxFamily server](http://download.tuxfamily.org/whatpeertube/).
to be later watched on your favorite video player

Production happened between 2 to 18 of May 2018. This is now a finished
project. This repository is there for reference for anyone wishing to
reuse the artworks.

## Software

Software used to create this video were:

* [GIMP](https://gimp.org) for raster images
* [Inkscape](https://inkscape.org) for vector images
* [Synfig Studio](https://www.synfig.org/) for the animation/motion
  design
* [Blender 3D](https://www.blender.org/) for the finale editing
* [Audacity](https://www.audacityteam.org/) for sound normalization

## Funding Libre Art and creative Free Software

The LILA non-profit is aimed at creating Libre Art for everyone to share
and reuse, while contributing code to Free Software as well.

The team behind this video happens to be [major contributors of GIMP,
and many more Free Software](https://www.openhub.net/accounts/Jehan/).
If you wish to fund them, you are welcome to do so:

* [Liberapay](https://liberapay.com/ZeMarmot)
* [Patreon](https://www.patreon.com/zemarmot/)
* [Tipeee](https://www.tipeee.com/zemarmot)
* [Generic funding page](https://film.zemarmot.net/en/donate)

If you wish to contribute to PeerTube, you are welcome to [join
PeerTube](https://joinpeertube.org/).

## Licenses

The film and all original resources are shared under [Creative Commons -
Attribution-ShareAlike 4.0
International](https://creativecommons.org/licenses/by-sa/4.0/) license.

In other words, feel free to download and share the video or any
artworks, but also reuse them, remix, and create derivated artworks from
these for any purpose, under the requirements of giving appropriate
credits for every author of the original art (while not suggesting they
endorse your own work in any way), and your derivative has to be shared
under the same license as well.

The voice audio is by Pouhiou, under Creative Commons -
Attribution-ShareAlike 4.0 International as well.

The music on the other hand is ["Red Step Forward" by Ken
Bushima](http://play.dogmazic.net/search.php?type=song#song.php?action=show_song&song_id=52491)
under Creative Commons - Attribution 3.0.

Finally the small 3D clip at 00:06 is "[Caminandes 3:
Llamigos](http://www.caminandes.com/)" by Blender Institude under
Creative Commons - Attribution 4.0.
